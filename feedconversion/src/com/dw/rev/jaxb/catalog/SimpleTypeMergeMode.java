//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.11.26 at 01:33:26 PM IST 
//


package com.dw.rev.jaxb.catalog;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for simpleType.MergeMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="simpleType.MergeMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="add"/>
 *     &lt;enumeration value="merge"/>
 *     &lt;enumeration value="remove"/>
 *     &lt;enumeration value="replace"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "simpleType.MergeMode")
@XmlEnum
public enum SimpleTypeMergeMode {


    /**
     * Add specified objects at the end of the existing list.
     * 
     */
    @XmlEnumValue("add")
    ADD("add"),

    /**
     * 
     *                         Update existing specified objects without changing their position in the list.
     *                         Add new specified objects at end of existing list.
     *                         Remove existing objects not specified in list.
     *                     
     * 
     */
    @XmlEnumValue("merge")
    MERGE("merge"),

    /**
     * Remove specified objects from the existing list.
     * 
     */
    @XmlEnumValue("remove")
    REMOVE("remove"),

    /**
     * Replace existing list with specified objects.
     * 
     */
    @XmlEnumValue("replace")
    REPLACE("replace");
    private final String value;

    SimpleTypeMergeMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SimpleTypeMergeMode fromValue(String v) {
        for (SimpleTypeMergeMode c: SimpleTypeMergeMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
