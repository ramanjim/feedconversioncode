//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.11.27 at 04:39:34 PM IST 
//


package com.dw.rev.piers.jaxb.pricebook;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for complexType.PriceBook complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="complexType.PriceBook">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.Header"/>
 *         &lt;element name="price-tables" type="{http://www.demandware.com/xml/impex/pricebook/2006-10-31}complexType.PriceTables" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "complexType.PriceBook", propOrder = {
    "header",
    "priceTables"
})
public class ComplexTypePriceBook {

    @XmlElement(required = true)
    protected ComplexTypeHeader header;
    @XmlElement(name = "price-tables")
    protected ComplexTypePriceTables priceTables;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link ComplexTypeHeader }
     *     
     */
    public ComplexTypeHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexTypeHeader }
     *     
     */
    public void setHeader(ComplexTypeHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the priceTables property.
     * 
     * @return
     *     possible object is
     *     {@link ComplexTypePriceTables }
     *     
     */
    public ComplexTypePriceTables getPriceTables() {
        return priceTables;
    }

    /**
     * Sets the value of the priceTables property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexTypePriceTables }
     *     
     */
    public void setPriceTables(ComplexTypePriceTables value) {
        this.priceTables = value;
    }

}
