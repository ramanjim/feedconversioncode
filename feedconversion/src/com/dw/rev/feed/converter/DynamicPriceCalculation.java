package com.dw.rev.feed.converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dw.rev.jaxb.catalog.Catalog;
import com.dw.rev.jaxb.catalog.ComplexTypeProduct;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttribute;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttributes;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypeAmountEntry;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypeHeader;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypePriceBook;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypePriceTable;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypePriceTables;
import com.dw.rev.piers.jaxb.pricebook.ObjectFactory;
import com.dw.rev.piers.jaxb.pricebook.Pricebooks;

public class DynamicPriceCalculation {

	private final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) {

		JAXBContext jaxbContext;
		JAXBContext context;

		try {

			Map msrpPricebookMap = null;
			Map mapPricebookMap = null;
			Map thresoldPricebookMap = null;
			{	
			jaxbContext = JAXBContext
					.newInstance("com.dw.rev.piers.jaxb.pricebook");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			Pricebooks priceXmlObject;
			priceXmlObject = (Pricebooks) unmarshaller
					.unmarshal(new FileInputStream("D:/sapProductFeed/ram/usd-list-prices.xml"));

			List<ComplexTypePriceBook> PriceBookList = priceXmlObject.getPricebook();
			for (ComplexTypePriceBook obj  : PriceBookList) {
				if(obj.getHeader().getPricebookId().equalsIgnoreCase("usd-list-prices")){
					msrpPricebookMap = getPriceBookMap(obj.getPriceTables());
					System.out.println(msrpPricebookMap);
				}else if (obj.getHeader().getPricebookId().equalsIgnoreCase("usd-sale-prices")){
					mapPricebookMap = getPriceBookMap(obj.getPriceTables());
					System.out.println(mapPricebookMap);
				}else if (obj.getHeader().getPricebookId().equalsIgnoreCase("thresold")){
					thresoldPricebookMap = getPriceBookMap(obj.getPriceTables());
					System.out.println(thresoldPricebookMap);
				}
			}
			}
			jaxbContext = JAXBContext
					.newInstance("com.dw.rev.jaxb.catalog");
			Unmarshaller unmarshaller1 = jaxbContext.createUnmarshaller();
			Catalog catalogXmlObject;
			catalogXmlObject = (Catalog) unmarshaller1
					.unmarshal(new FileInputStream("D:/sapProductFeed/ram/catalog.xml"));

			List<ComplexTypeProduct> productList = catalogXmlObject.getProduct();
			
			Pricebooks salePricebook = new Pricebooks();
			
			//Reporting logic
			int i=1;
			String filename="D:/sapProductFeed/ram/output/Report_Prices.xls" ;
			OutputStream fileOut = new FileOutputStream(new File(filename));
	        HSSFWorkbook workbook=new HSSFWorkbook();
	        HSSFSheet sheet =  workbook.createSheet("FirstSheet");  
	        HSSFRow rowhead =   sheet.createRow(0);
	        
	        rowhead.createCell(0).setCellValue("ProductId");
	        rowhead.createCell(1).setCellValue("MSRPPrice");
	        rowhead.createCell(2).setCellValue("MAPPPrice");
	        rowhead.createCell(3).setCellValue("Thresold price");
	        rowhead.createCell(4).setCellValue("SalePrice");
	        rowhead.createCell(5).setCellValue("%cutoff");
	        rowhead.createCell(6).setCellValue("$cutoff");
	        
			for (ComplexTypeProduct obj : productList) {
				SharedTypeSiteSpecificCustomAttributes custAttrs= obj.getCustomAttributes();
				//System.out.println(custAttrs.getCustomAttribute().size());
				if(custAttrs != null && custAttrs.getCustomAttribute() != null){
					//getListOfRetailersList
					List listOfRetailes = getListOfRetailersList(custAttrs);
					//	System.out.println("test"+listOfRetailes);
					if(listOfRetailes != null && ! listOfRetailes.isEmpty()){
						System.setProperty("http.proxyHost", "janua.revsolutionsinc.com");
						System.setProperty("http.proxyPort", "8080");
						DynamicPriceCalculation http = new DynamicPriceCalculation();
						System.out.println("Testing 1 - Send Http GET request");
						// getDynamicprice based on the Price Spider API
						Double dynamicPrice = http.getDynamicPrice(listOfRetailes,obj.getProductId());
						System.out.println("dynamicPrice"+dynamicPrice);

						//CalculateDynamicPrice
						if(dynamicPrice > 0){
							dynamicPrice= dynamicPrice +((dynamicPrice/100)*Math.random()*5);
							System.out.println("start 000");
							if(dynamicPrice > (Double)msrpPricebookMap.get(obj.getProductId())){
								dynamicPrice=(Double)msrpPricebookMap.get(obj.getProductId());
							}else if((Double)mapPricebookMap.get(obj.getProductId()) != 0 && dynamicPrice >= (Double)mapPricebookMap.get(obj.getProductId())){
								dynamicPrice=(Double)mapPricebookMap.get(obj.getProductId());
							}else if(dynamicPrice < (Double)thresoldPricebookMap.get(obj.getProductId())){
								dynamicPrice=(Double)thresoldPricebookMap.get(obj.getProductId());
							}
						}else{
							dynamicPrice=(Double)msrpPricebookMap.get(obj.getProductId());
						}

						System.out.println("start");
						System.out.println(dynamicPrice);

						
	
						ObjectFactory of = new ObjectFactory();

						ComplexTypePriceBook saleComplexTypePriceBook = of.createComplexTypePriceBook();
						ComplexTypePriceTables saleComplexTypePriceTables = new ComplexTypePriceTables();
						ComplexTypePriceTable SaleComplexTypePriceTable = new ComplexTypePriceTable();
						SaleComplexTypePriceTable.setProductId(obj.getProductId());
						ComplexTypeAmountEntry complexTypeAmountEntry = new ComplexTypeAmountEntry();
						complexTypeAmountEntry.setQuantity((BigDecimal.valueOf(1)));
						complexTypeAmountEntry.setValue((BigDecimal.valueOf(dynamicPrice)));
						SaleComplexTypePriceTable.getAmountOrPercentage().add(complexTypeAmountEntry);

						saleComplexTypePriceTables.getPriceTable().add(SaleComplexTypePriceTable);


						ComplexTypeHeader saleComplexTypeHeader = new ComplexTypeHeader();

						saleComplexTypeHeader.setPricebookId("SALE");


						System.out.println(SaleComplexTypePriceTable.getProductId());


						saleComplexTypeHeader.setCurrency("USD");


						saleComplexTypePriceBook.setHeader(saleComplexTypeHeader);


						saleComplexTypePriceBook.setPriceTables(saleComplexTypePriceTables);
						System.out.println(saleComplexTypePriceBook.getPriceTables().getPriceTable().size());
						

						salePricebook.getPricebook().add(saleComplexTypePriceBook);
						System.out.println(salePricebook.getPricebook().get(0).getHeader().getPricebookId());
						
						HSSFRow rowData =   sheet.createRow(i);
						
						Double threshHoldPrice = (Double)thresoldPricebookMap.get(obj.getProductId());
						
						Double perCutOfPrice = (100/(dynamicPrice-threshHoldPrice))*dynamicPrice;
						//Double perCutOfPrice = 50.0;
						Double dollorCutOff = (dynamicPrice-threshHoldPrice);
						
						rowData.createCell(0).setCellValue(obj.getProductId());
						rowData.createCell(1).setCellValue((Double) msrpPricebookMap.get(obj.getProductId()));
						rowData.createCell(2).setCellValue((Double)mapPricebookMap.get(obj.getProductId()));
						rowData.createCell(3).setCellValue(threshHoldPrice);
						rowData.createCell(4).setCellValue(dynamicPrice);
						rowData.createCell(5).setCellValue(perCutOfPrice);
						rowData.createCell(6).setCellValue(dollorCutOff);
						
						i++;
					}
				}				
				
			}
			
			JAXBContext context1 = JAXBContext.newInstance("com.dw.rev.piers.jaxb.pricebook");
			Marshaller marshaller = context1.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "UTF-8");
			marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
			marshaller.marshal(salePricebook, new FileOutputStream(new File("D:/sapProductFeed/ram/output/marshalledSalePricebook.xml")));
			
			
			//Creating the xls sheet
	        workbook.write(fileOut);
	        System.out.println(workbook);
		} catch (JAXBException e) {
			e.printStackTrace();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// getListOfRetailersList
	private static List getListOfRetailersList(
			SharedTypeSiteSpecificCustomAttributes customAttribute) {
		List listOfRetailes = new ArrayList();
		for (SharedTypeSiteSpecificCustomAttribute custAttr :  customAttribute.getCustomAttribute()) {
			if(custAttr.getAttributeId().equalsIgnoreCase("disable_dynamic_price") && custAttr.getContent().get(0) == null && "false".equalsIgnoreCase(custAttr.getContent().get(0).toString())){
				return null;
			}
			//System.out.println("hhh"+obj1.getAttributeId());
			if(custAttr.getAttributeId().equalsIgnoreCase("listOfCompetitiveRetailers")){
				for(Object value : custAttr.getContent()) {
					if(value instanceof JAXBElement){
						listOfRetailes.add(((JAXBElement) value).getValue());
					}
				}
			}
			System.out.println(listOfRetailes);	
		}
		return listOfRetailes;
	}

	/**
	 * Get priceMaps for MSRP,MAP & Thresold
	 * @param priceTables
	 * @return
	 */
	private static Map getPriceBookMap(ComplexTypePriceTables priceTables) {

		Map<String,Double> priceMap = new HashMap<String,Double>();
		for (ComplexTypePriceTable priceTable :priceTables.getPriceTable()) {
			//				System.out.println(priceTable.getOnlineFrom()+"date"+ priceTable.getOnlineTo());
			//				priceTable.getOnlineTo();
			for (Object price: priceTable.getAmountOrPercentage()){
				priceMap.put(priceTable.getProductId(),Double.parseDouble(((((ComplexTypeAmountEntry)price).getValue()).toString())));
			}
		}
		return priceMap;

	}

	/**
	 * getPriceSpiderAPIPrice : getDynamicprice based on the Price Spider API
	 * @param listOfRetailes
	 * @return
	 * @throws IOException 
	 * @throws JSONException 
	 * @throws Exception
	 */
	private Double getDynamicPrice(List listOfRetailes,String productId) throws IOException, JSONException {
		//TODO 
		productId="US_PS410034";
		String url = "http://api.pricespider.com/v2/restjson.svc/GetOnlineSellers?apiConfigurationId=f3dc9038-27c7-4300-8ab0-18684955d12e&skuList=" + productId;

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		//System.out.println("\nSending 'GET' request to URL : " + url);
		//System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		//print result
		JSONObject jsonObj = new JSONObject(response.toString());
		Iterator<?> keys = jsonObj.keys();
		List<Double> priceList = new ArrayList<Double>();
		while( keys.hasNext() ){
			String key = (String)keys.next();

			if(key.equalsIgnoreCase("ProductsOnlineSellers")){
				JSONArray sellersList=(JSONArray) jsonObj.get("ProductsOnlineSellers");
				JSONObject sellers = (JSONObject) sellersList.get(0);
				JSONArray sellersList1 =(JSONArray) sellers.get("Sellers");
				for(int i = 0; i < sellersList1.length(); i++)
				{
					JSONObject object = (JSONObject) sellersList1.get(i);
					if(listOfRetailes.contains(object.get("SellerName"))){
						//						System.out.println(object.get("SellerName"));
						priceList.add((Double) object.get("Price"));
					}
				}
				Collections.sort(priceList);
			}
		}

		Double price=priceList.get(0);
		System.out.println(price);
		return price;
	}
}
