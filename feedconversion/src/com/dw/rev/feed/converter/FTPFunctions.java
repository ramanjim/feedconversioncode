package com.dw.rev.feed.converter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;


public class FTPFunctions {

	// Creating FTP Client instance
	FTPClient ftp = null;

	// Constructor to connect to the FTP Server
	public FTPFunctions(String host, int port, String username, String password) throws Exception{

		//System.setProperty("http.proxyHost", "janua.revsolutionsinc.com");
		//System.setProperty("http.proxyPort", "8080");

		// get an ftpClient object  
		ftp = new FTPClient();  

		try {
			// pass directory path on server to connect  
			ftp.connect(host);  

			// pass username and password, returned true if authentication is  
			// successful  
			boolean login = ftp.login(username, password);  

			if (login) {  
				System.out.println("Connection established...");
				System.out.println("Status: "+ftp.getStatus());

				// logout the user, returned true if logout successfully  
				/*boolean logout = ftp.logout();
		          if (logout) {  
		           System.out.println("Connection close...");  
		          }*/
			} else {
				System.out.println("Connection fail...");  
			}			
			int reply;	        	        
			reply = ftp.getReplyCode();
			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				throw new Exception("Exception in connecting to FTP Server");
			}	        
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.enterLocalPassiveMode();    

		} catch (SocketException e) {  
			e.printStackTrace();  
		} catch (IOException e) {  
			e.printStackTrace();  
		}  

	}    

	// Method to upload the File on the FTP Server
	public void uploadFTPFile(String localFileFullName, String fileName, String hostDir)
			throws Exception {
		try {
			InputStream input = new FileInputStream(new File(localFileFullName));

			this.ftp.storeFile(hostDir + fileName, input);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	// Download the FTP File from the FTP Server
	public void downloadFTPFile(String source, String destination) {		
	        try {
	        	OutputStream  fos = new FileOutputStream(destination);
	            this.ftp.retrieveFile(source, fos);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

	// list the files in a specified directory on the FTP
	public boolean listFTPFiles(String directory, String fileName) throws IOException {
		// lists files and directories in the current working directory
		boolean verificationFilename = false;        
		FTPFile[] files = ftp.listFiles(directory);
		for (FTPFile file : files) {
			String details = file.getName();                
			System.out.println(details);            
			if(details.equals(fileName))
			{
				System.out.println("Correct Filename");
				verificationFilename=details.equals(fileName);
			}
		}  

		return verificationFilename;
	}

	// Disconnect the connection to FTP
	public void disconnect(){
		if (this.ftp.isConnected()) {
			try {
				this.ftp.logout();
				this.ftp.disconnect();
			} catch (IOException f) {
				// do nothing as file is already saved to server
			}
		}
	}

	// Main method to invoke the above methods
	public static void main(String[] args) {
		try {
			FTPFunctions ftpobj = new FTPFunctions("54.165.252.223", 22, "Niran.Terapalli@ext.us.panasonic.com", "panasonic11");
			ftpobj.uploadFTPFile("D:/sapProductFeed/toDemandware/recycle_fees_12-16-2014.xml", "recycle_fees_12-16-2014.xml", "/demandware/out/pricing/");
			ftpobj.downloadFTPFile("recycle_fees_12-16-2014.xml", "/demandware/in/pricing/");
			System.out.println("FTP File uploaded successfully");
			//boolean result = ftpobj.listFTPFiles("/users/shruti", "shruti.txt");
			//System.out.println(result);
			ftpobj.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}