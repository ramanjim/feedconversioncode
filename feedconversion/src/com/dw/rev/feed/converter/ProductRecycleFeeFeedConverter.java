package com.dw.rev.feed.converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.dw.rev.jaxb.catalog.Catalog;
import com.dw.rev.jaxb.catalog.ComplexTypeProduct;
import com.dw.rev.jaxb.catalog.ObjectFactory;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttribute;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttributes;


public class ProductRecycleFeeFeedConverter {
	
	public static void main(String[] args) {
		String csvFile = "D:/sapProductFeed/fromDemandware/pricing_recycle_fees.txt";		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\\|";
		
		
		try {
			br = new BufferedReader(new FileReader(csvFile));
			JAXBContext context = JAXBContext.newInstance("com.dw.rev.jaxb.catalog");
			Marshaller marshaller = context.createMarshaller();
	        marshaller.setProperty("jaxb.encoding", "UTF-8");
	        marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
	        ObjectFactory objectFactory = new ObjectFactory();	        
	        Catalog catalog = objectFactory.createCatalog();	        
	        catalog.setCatalogId("electronics");	        
	        ComplexTypeProduct complexTypeProduct = null ;
	        SharedTypeSiteSpecificCustomAttribute custAttr = null;
	        
	        String keyString = "MODEL_NO|Region|ProductCode|Amount|ValidFrom|ValidTo";
	        String[] keys = keyString.split(cvsSplitBy);
	        /*for(int i=0; i< keys.length; i++){
	        	keys[i] = keys[i].replaceAll("[^=\\da-zA-Z-_]","");        	
	        }*/
	        
	        // price mapping required
	        // MODEL # Region Product Code	Amount Valid From Valid To		         	         
	        	        
	        int i=0;
			while ((line = br.readLine()) != null  ) {								
		        if (i++ > 2 && !line.equals("")) {
					// use comma as separator
					String[] string = line.split(cvsSplitBy);
					SharedTypeSiteSpecificCustomAttributes custAttrs = new SharedTypeSiteSpecificCustomAttributes();
					if(string.length > 0 && string[0] != null   && !string[0].equals("")){
						complexTypeProduct = new ComplexTypeProduct();
						if(string.length>0 && string[0]!= null   && !string[0].equals("")){							
							complexTypeProduct.setProductId(string[0]);							
						}
						
						int length = Math.min(keys.length,string.length);
						for(int j = 1; j < length; j++){
							if((keys.length > 0 && string.length>0) && (keys[j]!= null && string[j]!= null) && (!keys[j].equals("") && !string[j].equals(""))){
								
								if((j == 4) || (j == 5)) {
									GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"));
									gregorianCalendar.set(Integer.parseInt(string[j].split("-")[2]), Integer.parseInt(string[j].split("-")[1])-1, Integer.parseInt(string[j].split("-")[0]),0,0,0);
									XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);									
									custAttr = new SharedTypeSiteSpecificCustomAttribute();
									custAttr.setAttributeId(keys[j]);
									custAttr.getContent().add(xMLGregorianCalendar.toString());
									custAttrs.getCustomAttribute().add(custAttr);									
								} else {
									custAttr = new SharedTypeSiteSpecificCustomAttribute();
									String key = keys[j].replaceAll("[^=\\da-zA-Z-]","");
									custAttr.setAttributeId(key);
									custAttr.getContent().add(string[j]);
									custAttrs.getCustomAttribute().add(custAttr);									
								}								
							}
						}
					}
					complexTypeProduct.setCustomAttributes(custAttrs);
					catalog.getProduct().add(complexTypeProduct);
		        }
			}
			
	        //catalog.getProduct().add(complexTypeProduct);
			Date date = new Date();		
			String formatedDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
			String fileName = "recycle_fees_"+formatedDate;
	        marshaller.marshal(catalog, new FileOutputStream(new File("D:/sapProductFeed/toDemandware/"+fileName+".xml")));
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	}
}
