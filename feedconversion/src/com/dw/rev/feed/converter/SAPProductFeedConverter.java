package com.dw.rev.feed.converter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.dw.rev.jaxb.catalog.Catalog;
import com.dw.rev.jaxb.catalog.ComplexTypeProduct;
import com.dw.rev.jaxb.catalog.ObjectFactory;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttribute;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttributes;


public class SAPProductFeedConverter {
	public static void main(String[] args) {
		String csvFile = "D:/sapProductFeed/fromDemandware/product_20141121230025.txt";		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\t";
		
		try {
			br = new BufferedReader(new FileReader(csvFile));
			JAXBContext context = JAXBContext.newInstance("com.dw.rev.jaxb.catalog");
			Marshaller marshaller = context.createMarshaller();
	        marshaller.setProperty("jaxb.encoding", "UTF-8");
	        marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
	        ObjectFactory objectFactory = new ObjectFactory();	        
	        Catalog catalog = objectFactory.createCatalog();	        
	        catalog.setCatalogId("electronics");	        
	        ComplexTypeProduct complexTypeProduct = null ;
	        SharedTypeSiteSpecificCustomAttribute custAttr = null;
	        
	        String keyString = "Material	Old material number	Material Description	Height	Width	Length	Gross Weight	Weight Unit	CART_FACTOR	International Article Number (EAN/UPC)	Division	Material Group 	Material Group 1	Material Group 1 description	Material Group Description	PH1	PH1 Description	PH2	PH2 Description	Material Number Used by Vendor	Material Activity End date	Material block - inventory, procurement	Material block description	Material block - Selling, Shipping	Valid from date	Material block description	Valid from date	Material Type	Material Type Description	Product Code	Tax Flag";
	        String[] keys = keyString.split("\t");
	        for(int i=0; i< keys.length; i++){
	        	keys[i] = keys[i].replaceAll("[^=\\da-zA-Z-_]","");        	
	        }
	        
	        // product mapping required
	        /*Material	Old material number	Material Description	Height	
	         * Width	Length	Gross Weight	Weight Unit	CART_FACTOR	
	         * International Article Number (EAN/UPC)	Division	Material Group 	Material Group 1	Material Group 1 description	
	         * Material Group Description	PH1	PH1 Description	PH2	PH2 Description	
	         * Material Number Used by Vendor	Material Activity End date	Material block - inventory, procurement	Material block description	Material block - Selling, Shipping	
	         * Valid from date	Material block description	Valid from date	Material Type	Material Type Description
	         */
	        	        
	        int i=0;
			while ((line = br.readLine()) != null  ) {								
		        if (i++ > 2 ) {
					// use comma as separator
					String[] string = line.split(cvsSplitBy);
					SharedTypeSiteSpecificCustomAttributes custAttrs = new SharedTypeSiteSpecificCustomAttributes();
					if(string.length > 0 && string[0] != null   && !string[0].equals("")){
						complexTypeProduct = new ComplexTypeProduct();
						if(string.length>0 && string[0]!= null   && !string[0].equals("")){							
							complexTypeProduct.setProductId(string[0]);							
						}
						int length = Math.min(keys.length,string.length);
						for(int j = 1; j < length; j++){
							if((keys.length > 0 && string.length>0) && (keys[j]!= null && string[j]!= null) && (!keys[j].equals("") && !string[j].equals(""))){								
								String key = keys[j].replaceAll("[^=\\da-zA-Z-]","");
								
								if(key.equals("Validfromdate")) {
									GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"));
									gregorianCalendar.set(Integer.parseInt(string[j].split("/")[2]), Integer.parseInt(string[j].split("/")[0])-1, Integer.parseInt(string[j].split("/")[1]),0,0,0);
									XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);									
									custAttr = new SharedTypeSiteSpecificCustomAttribute();
									custAttr.setAttributeId(key);
									custAttr.getContent().add(xMLGregorianCalendar.toString());
									custAttrs.getCustomAttribute().add(custAttr);									
								} else {								
									custAttr = new SharedTypeSiteSpecificCustomAttribute();									
									custAttr.setAttributeId(key);
									custAttr.getContent().add(string[j]);
									custAttrs.getCustomAttribute().add(custAttr);
								}
							}
						}
					}
					complexTypeProduct.setCustomAttributes(custAttrs);
					catalog.getProduct().add(complexTypeProduct);
		        }
			}
			
			Date date = new Date();		
			String formatedDate = new SimpleDateFormat("MM-dd-yyyy").format(date);
			String fileName = "SapDwProduct_"+formatedDate;
	        marshaller.marshal(catalog, new FileOutputStream(new File("D:/sapProductFeed/toDemandware/"+fileName+".xml")));
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
	}
}
