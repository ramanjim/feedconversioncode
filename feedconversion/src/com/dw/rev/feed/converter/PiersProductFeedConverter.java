package com.dw.rev.feed.converter;

import java.awt.image.BufferStrategy;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dw.rev.jaxb.catalog.Catalog;
import com.dw.rev.jaxb.catalog.ComplexTypeProduct;
import com.dw.rev.jaxb.catalog.ObjectFactory;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttribute;
import com.dw.rev.jaxb.catalog.SharedTypeSiteSpecificCustomAttributes;
import com.sap.rev.jaxb.piers.Data;
import com.sap.rev.jaxb.piers.DataGroup;
import com.sap.rev.jaxb.piers.ItemContents;
import com.sap.rev.jaxb.piers.ItemContents.ItemContent;

public class PiersProductFeedConverter {

	private static String dataGroupFlg="level1";
	private static int j=0;
	private static StringBuffer feedData = new StringBuffer();
	public static void main(String[] args) {
		
		JAXBContext jaxbContext;
		JAXBContext context;
		
			try {
				jaxbContext = JAXBContext
						.newInstance("com.sap.rev.jaxb.piers");
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			
			ItemContents xmlObject;
				xmlObject = (ItemContents) unmarshaller
						.unmarshal(new FileInputStream("C:/Users/r0135/Desktop/xmlconversion/piers-americaItemIF_20140717162330.xml"));
			
			try {
				JSONObject resultJson = parseObject(xmlObject.getDataOrDataGroupOrItemContent(),0);
				System.out.println( resultJson);
				System.out.println("feed data" +  feedData.toString());
				context = JAXBContext.newInstance("com.dw.rev.jaxb.catalog");
				Marshaller marshaller = context.createMarshaller();
		        marshaller.setProperty("jaxb.encoding", "UTF-8");
		        marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
		        ComplexTypeProduct complexTypeProduct = new ComplexTypeProduct();
		        complexTypeProduct.setProductId((String)resultJson.get("11||Unified Model Number"));
		        SharedTypeSiteSpecificCustomAttributes custAttrs = new SharedTypeSiteSpecificCustomAttributes();
		        SharedTypeSiteSpecificCustomAttribute custAttr = new SharedTypeSiteSpecificCustomAttribute();
		        custAttr.setAttributeId("TechnicalFeature");
		        custAttr.getContent().add(feedData.toString());
		       
		        custAttrs.getCustomAttribute().add(custAttr);
		        complexTypeProduct.setCustomAttributes(custAttrs);
		        Catalog catalog = new ObjectFactory().createCatalog();
		        catalog.setCatalogId(resultJson.getString("catalogName"));
		        catalog.getProduct().add(complexTypeProduct);
		        marshaller.marshal(catalog, new FileOutputStream(new File("C:/Users/r0135/Desktop/xmlconversion/output/marshalledZoo.xml")));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		

	}

	private static JSONObject parseObject(List xmlObjectList, int innerLevel) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		int i=0;
		for (Object obj : xmlObjectList) {
			i++;
			
			
			if(obj instanceof Data){
				j++;
				Data dataobj = (Data)obj;
				if( dataobj.getAttributeName() != null && ( (dataobj.getAttributeName().equals("NOTE") && jsonObject.has("NOTE")) ||  (dataobj.getAttributeName().equals("TRADEMARK NOTICE") && jsonObject.has("TRADEMARK NOTICE")) )){
					jsonObject.put(dataobj.getAttributeName(), (jsonObject.get(dataobj.getAttributeName())+"/"+ dataobj.getValue()));
				}else if( dataobj.getAttributeName() != null && ( (dataobj.getAttributeName().equals("NOTE") && !jsonObject.has("NOTE")) ||  (dataobj.getAttributeName().equals("TRADEMARK NOTICE") && !jsonObject.has("TRADEMARK NOTICE")) )){
					jsonObject.put( dataobj.getAttributeName(), dataobj.getValue());
				}else{
					jsonObject.put(((innerLevel*10)+i)+"||" +dataobj.getAttributeName(), dataobj.getValue());
				}
				feedData.append(dataobj.getAttributeName()).append("|").append(dataobj.getValue()).append("|").append("\n");
				continue;
			}
			if(obj instanceof DataGroup){
				JSONObject localjsonObject = new JSONObject();
				JSONObject localjsonObject2 = new JSONObject();
				DataGroup dataobj = (DataGroup)obj;
				if( dataobj.getAttributeName().equals("TechnicalFeature") || !dataGroupFlg.equals("level1")){
					dataGroupFlg="level2";
//					feedData.append("\n").append("++").append(dataobj.getAttributeName()).append("||").append(innerLevel);
					feedData.append(dataobj.getAttributeName()).append("||");
					if(innerLevel>0 && innerLevel<10){
						feedData.append("level1").append("\n");
					}else if(innerLevel>10 && innerLevel<100){
						feedData.append("level2").append("\n");
					}else if(innerLevel>=100 && innerLevel<1000){
						feedData.append("level3").append("\n");
					}else if(innerLevel>=1000 && innerLevel<10000){
						feedData.append("level4").append("\n");
					}else {
						//feedData.append("sub").append("\n");
					}
					System.out.println(dataobj.getAttributeName()+"-------------");
					localjsonObject = parseObject(dataobj.getData(), (innerLevel*10)+1);
						localjsonObject2 = parseObject(dataobj.getDataGroup(), (innerLevel*10)+1);
						if(localjsonObject2 != null && localjsonObject2.length() > 0){
							
							Iterator<?> keys = localjsonObject2.keys();
					        while( keys.hasNext() ){
					            String key = (String)keys.next();
					            	localjsonObject.put( key, localjsonObject2.get(key));
					        }
						}
					jsonObject.put(((innerLevel*10)+i)+"||" +dataobj.getAttributeName(),localjsonObject) ;
					
				}
				continue;
			}
			
			if(obj instanceof ItemContent){
				JSONObject localjsonObject = new JSONObject();
				JSONObject localjsonObject2 = new JSONObject();
				System.out.println("catalog name : "+((ItemContent) obj).getCatalogName());
				ItemContent dataobj = (ItemContent)obj;
				jsonObject.put("catalogName", ((ItemContent) obj).getCatalogName()) ;
				
				localjsonObject = parseObject(dataobj.getData(), (innerLevel*10)+1);
				if(localjsonObject != null){
					Iterator<?> keys = localjsonObject.keys();
			        while( keys.hasNext() ){
			            String key = (String)keys.next();
			            jsonObject.put( key, localjsonObject.get(key));
			        }
				}
				localjsonObject2 = parseObject(dataobj.getDataGroup(), (innerLevel*10)+1);
				if(dataobj.getDataGroup() != null){
					Iterator<?> keys = localjsonObject2.keys();
			        while( keys.hasNext() ){
			            String key = (String)keys.next();
			            jsonObject.put( key, localjsonObject2.get(key));
			        }
				}
			}
		}
		return jsonObject;
		
	}

}
