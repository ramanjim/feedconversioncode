package com.dw.rev.feed.converter;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.dw.rev.piers.jaxb.pricebook.ComplexTypeAmountEntry;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypeHeader;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypePriceBook;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypePriceTable;
import com.dw.rev.piers.jaxb.pricebook.ComplexTypePriceTables;
import com.dw.rev.piers.jaxb.pricebook.ObjectFactory;
import com.dw.rev.piers.jaxb.pricebook.Pricebooks;
 
public class SAPPriceBookFeedConverter {
 
  public static void main(String[] args) {
 
	SAPPriceBookFeedConverter obj = new SAPPriceBookFeedConverter();
	obj.run();
 
  }
 
  public void run() {
 
	String csvFile = "C:/Users/r0135/Desktop/xmlconversion/pricing_20141122000104.txt";
	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = "\\|";
 
	try {
 
		br = new BufferedReader(new FileReader(csvFile));
		JAXBContext context = JAXBContext.newInstance("com.dw.rev.piers.jaxb.pricebook");
		Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("jaxb.encoding", "UTF-8");
        marshaller.setProperty("jaxb.formatted.output",Boolean.TRUE);
        
        ObjectFactory of = new ObjectFactory();
        
        ComplexTypePriceBook msrpComplexTypePriceBook = of.createComplexTypePriceBook();
        ComplexTypePriceBook mapComplexTypePriceBook = of.createComplexTypePriceBook();
        ComplexTypePriceBook thresholdComplexTypePriceBook = of.createComplexTypePriceBook();
        ComplexTypePriceTables msrpComplexTypePriceTables = new ComplexTypePriceTables();
        ComplexTypePriceTables mapComplexTypePriceTables = new ComplexTypePriceTables();
        ComplexTypePriceTables thresholdComplexTypePriceTables = new ComplexTypePriceTables();
       
        /*Model #	MSRP	
         * "MSRP validfrom"	"
         * MSRP     valid to"	
         * MAP	"MAP    validfrom"	
         * "MAP   valid to"	
         * Threshold	
        "Threshold  valid from"	
        "Threshold  valid to"*/
        int i=0;
		while ((line = br.readLine()) != null  ) {
		        if (i++ > 2 ) {
		        		
		        	ComplexTypePriceTable msrpComplexTypePriceTable = new ComplexTypePriceTable();
		        	ComplexTypePriceTable mapComplexTypePriceTable = new ComplexTypePriceTable();
		        	ComplexTypePriceTable thresholdComplexTypePriceTable = new ComplexTypePriceTable();
					// use comma as separator
					String[] string = line.split(cvsSplitBy);
					if(string.length>0 && string[0]!= null   && !string[0].equals("")){
					msrpComplexTypePriceTable.setProductId(string[0]);
					mapComplexTypePriceTable.setProductId(string[0]);
					thresholdComplexTypePriceTable.setProductId(string[0]);
					}
					if(string.length>1 && string[1]!= null   && !string[1].equals("")){
						ComplexTypeAmountEntry complexTypeAmountEntry = new ComplexTypeAmountEntry();
						complexTypeAmountEntry.setQuantity((BigDecimal.valueOf(1)));
						complexTypeAmountEntry.setValue((BigDecimal.valueOf(Double.valueOf(string[1]))));
						msrpComplexTypePriceTable.getAmountOrPercentage().add(complexTypeAmountEntry);
						}
					if(string.length>2 && string[2]!= null  && !string[2].equals("")){
						GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"));
						gregorianCalendar.set(Integer.parseInt(string[2].split("/")[2]), Integer.parseInt(string[2].split("/")[0])-1, Integer.parseInt(string[2].split("/")[1]),0,0,0);
						XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
						msrpComplexTypePriceTable.setOnlineFrom(xMLGregorianCalendar);
					}
					if(string.length>3 && string[3]!= null  && !string[3].equals("")){
						GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"), new Locale("en", "US"));
						gregorianCalendar.set(Integer.parseInt(string[3].split("/")[2]), Integer.parseInt(string[3].split("/")[0])-1, Integer.parseInt(string[3].split("/")[1]),0,0,0);
						XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
						msrpComplexTypePriceTable.setOnlineTo(xMLGregorianCalendar);
					}
					if(string.length>4 && string[4]!= null  && !string[4].equals("")){
						ComplexTypeAmountEntry complexTypeAmountEntry = new ComplexTypeAmountEntry();
						complexTypeAmountEntry.setQuantity((BigDecimal.valueOf(1)));
						complexTypeAmountEntry.setValue((BigDecimal.valueOf(Double.valueOf(string[4]))));
						mapComplexTypePriceTable.getAmountOrPercentage().add(complexTypeAmountEntry);
					}
					if(string.length>5 && string[5]!= null  && !string[5].equals("")){
						GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"), new Locale("en", "US"));
						gregorianCalendar.set(Integer.parseInt(string[5].split("/")[5]), Integer.parseInt(string[5].split("/")[0])-1, Integer.parseInt(string[5].split("/")[1]),0,0,0);
						XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
						mapComplexTypePriceTable.setOnlineFrom(xMLGregorianCalendar);
					}
					if(string.length>6 && string[6]!= null  && !string[6].equals("")){
						GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"), new Locale("en", "US"));
						gregorianCalendar.set(Integer.parseInt(string[6].split("/")[2]), Integer.parseInt(string[6].split("/")[0])-1, Integer.parseInt(string[6].split("/")[1]),0,0,0);
						XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
						mapComplexTypePriceTable.setOnlineTo(xMLGregorianCalendar);
					}
					if(string.length>7 && string[7]!= null  && !string[7].equals("")){
						ComplexTypeAmountEntry complexTypeAmountEntry = new ComplexTypeAmountEntry();
						complexTypeAmountEntry.setQuantity((BigDecimal.valueOf(1)));
						complexTypeAmountEntry.setValue((BigDecimal.valueOf(Double.valueOf(string[7]))));
						thresholdComplexTypePriceTable.getAmountOrPercentage().add(complexTypeAmountEntry);
					}
					
					if(string.length>8 && string[8]!= null  && !string[8].equals("")){
						System.out.print(" validfrom # :" + string[8]);
						GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"));
						gregorianCalendar.set(Integer.parseInt(string[8].split("/")[2]), Integer.parseInt(string[8].split("/")[0])-1, Integer.parseInt(string[8].split("/")[1]),0,0,0);
						XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
						thresholdComplexTypePriceTable.setOnlineFrom(xMLGregorianCalendar);
					}
					if(string.length>9 && string[9]!= null  && !string[9].equals("")){
						GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("Etc/UTC"), new Locale("en", "US"));
						gregorianCalendar.set(Integer.parseInt(string[9].split("/")[2]), Integer.parseInt(string[9].split("/")[0])-1, Integer.parseInt(string[9].split("/")[1]),0,0,0);
						XMLGregorianCalendar  xMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
						thresholdComplexTypePriceTable.setOnlineTo(xMLGregorianCalendar);
					}

					if(msrpComplexTypePriceTable.getAmountOrPercentage().size()>0){
						msrpComplexTypePriceTables.getPriceTable().add(msrpComplexTypePriceTable);
					}
					if(mapComplexTypePriceTable.getAmountOrPercentage().size()>0){
						mapComplexTypePriceTables.getPriceTable().add(mapComplexTypePriceTable);
					}
					if(thresholdComplexTypePriceTable.getAmountOrPercentage().size()>0){
						thresholdComplexTypePriceTables.getPriceTable().add(thresholdComplexTypePriceTable);
					}
					 
		        }
 
		}
		ComplexTypeHeader msrpComplexTypeHeader = new ComplexTypeHeader();
		ComplexTypeHeader mapComplexTypeHeader = new ComplexTypeHeader();
		ComplexTypeHeader thresholdComplexTypeHeader = new ComplexTypeHeader();
		
		msrpComplexTypeHeader.setPricebookId("MSRP");
		mapComplexTypeHeader.setPricebookId("MAP");
		thresholdComplexTypeHeader.setPricebookId("Threshold");
		
		msrpComplexTypeHeader.setCurrency("USD");
		mapComplexTypeHeader.setCurrency("USD");
		thresholdComplexTypeHeader.setCurrency("USD");
		
		msrpComplexTypePriceBook.setHeader(msrpComplexTypeHeader);
		mapComplexTypePriceBook.setHeader(mapComplexTypeHeader);
		thresholdComplexTypePriceBook.setHeader(thresholdComplexTypeHeader);
		
        msrpComplexTypePriceBook.setPriceTables(msrpComplexTypePriceTables);
        mapComplexTypePriceBook.setPriceTables(mapComplexTypePriceTables);
        thresholdComplexTypePriceBook.setPriceTables(thresholdComplexTypePriceTables);
        
        Pricebooks pricebook = new Pricebooks();
        if(msrpComplexTypePriceBook.getPriceTables().getPriceTable().size()>0){
        	pricebook.getPricebook().add(msrpComplexTypePriceBook);
        }
        if(mapComplexTypePriceBook.getPriceTables().getPriceTable().size()>0){
        	pricebook.getPricebook().add(mapComplexTypePriceBook);
        }
        if(thresholdComplexTypePriceBook.getPriceTables().getPriceTable().size()>0){
        	pricebook.getPricebook().add(thresholdComplexTypePriceBook);
        }
        
        
        marshaller.marshal(pricebook, new FileOutputStream(new File("C:/Users/r0135/Desktop/xmlconversion/output/marshalledPricebook.xml")));
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (JAXBException e) {
		e.printStackTrace();
	} catch (DatatypeConfigurationException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
 
  }
 
}